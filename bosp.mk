
ifdef CONFIG_CONTRIB_OCVDEMO

# Targets provided by this project
.PHONY: ocvdemo clean_ocvdemo

# Add this to the "contrib_testing" target
contrib: ocvdemo
clean_contrib: clean_ocvdemo

MODULE_CONTRIB_USER_OCVDEMO=contrib/user/ocvdemo

ocvdemo: external
	@echo
	@echo "==== Building OCVDemo ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_USER_OCVDEMO)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_USER_OCVDEMO)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_OCVDEMO)/build/$(BUILD_TYPE) && \
		CXX=$(CXX) cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_OCVDEMO)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_ocvdemo:
	@echo
	@echo "==== Clean-up OCVDemo ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/bbque-ocvdemo ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/OCVDemo*; \
		rm -f $(BUILD_DIR)/usr/bin/bbque-ocvdemo*
	@rm -rf $(MODULE_CONTRIB_USER_OCVDEMO)/build
	@echo

else # CONFIG_CONTRIB_OCVDEMO

ocvdemo:
	$(warning contib OCVDemo module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_OCVDEMO

